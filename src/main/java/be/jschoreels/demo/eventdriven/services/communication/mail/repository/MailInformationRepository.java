package be.jschoreels.demo.eventdriven.services.communication.mail.repository;

import be.jschoreels.demo.eventdriven.services.communication.mail.domain.MailInformation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MailInformationRepository extends CrudRepository<MailInformation, String> {
}
