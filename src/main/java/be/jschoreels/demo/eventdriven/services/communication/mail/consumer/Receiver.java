package be.jschoreels.demo.eventdriven.services.communication.mail.consumer;

import be.jschoreels.demo.eventdriven.services.alerter.api.event.AlertTriggered;
import be.jschoreels.demo.eventdriven.services.communicate.api.domain.Message;
import be.jschoreels.demo.eventdriven.services.communicate.api.event.MessageSent;
import be.jschoreels.demo.eventdriven.services.communication.mail.domain.MailInformation;
import be.jschoreels.demo.eventdriven.services.communication.mail.repository.MailInformationRepository;
import be.jschoreels.demo.eventdriven.services.pricetracking.api.event.ItemPriceDecreased;
import be.jschoreels.demo.eventdriven.usermanagement.event.AccountCreated;
import be.jschoreels.demo.eventdriven.usermanagement.event.AccountDeleted;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Component
public class Receiver {

    public static Logger logger = LoggerFactory.getLogger(Receiver.class);

    private final JmsTemplate jmsTemplate;

    private final MailInformationRepository mailInformationRepository;

    private Map<Class, Function<Object, String>> messagesPerAlertRootEvent = ImmutableMap.of(
            ItemPriceDecreased.class,
            (Object event) -> {
                ItemPriceDecreased itemPriceDecreased = new ObjectMapper().convertValue(event, ItemPriceDecreased.class);
                return String.format("The item %s has dropped below your price alert. The current price is %s",
                        itemPriceDecreased.getItem(), itemPriceDecreased.getNewPrice());
            }
    );

    @Autowired
    public Receiver(JmsTemplate jmsTemplate, MailInformationRepository mailInformationRepository) {
        this.jmsTemplate = jmsTemplate;
        this.mailInformationRepository = mailInformationRepository;
    }


    @JmsListener(destination = "Consumer.CommunicationMail:MessageSent.VirtualTopic.Alert:AlertTriggered", containerFactory = "queueListenerFactory")
    public void receiveMessageItemPriceDecreased(AlertTriggered alertTriggered) {
        logger.info("Received <" + alertTriggered.toString() + ">");
        Optional<MailInformation> optionalMailInformation = mailInformationRepository.findById(alertTriggered.getUser());
        if (optionalMailInformation.isPresent()) {
            MailInformation mailInformation = optionalMailInformation.get();
            String messageToSent = messagesPerAlertRootEvent.get(
                    alertTriggered.getRootEventType()).apply(alertTriggered.getRootEventPayload());
            logger.info("Sending mail to user {} at email {} with body : {}",
                    mailInformation.getName(),
                    mailInformation.getMail(),
                    messageToSent);
            jmsTemplate.convertAndSend("VirtualTopic.Communication:MessageSent", MessageSent.newBuilder()
                    .withMessage(Message.newBuilder()
                            .withBody(messageToSent)
                            .withMessageType("mail")
                            .build())
                    .withUser(mailInformation.getName())
                    .withSentTime(ZonedDateTime.now())
                    .build());
        } else {
            logger.info("No mail was found for user {}", alertTriggered.getUser());
        }
    }

    @JmsListener(destination = "Consumer.CommunicationMail.VirtualTopic.User:AccountCreated",
            containerFactory = "queueListenerFactory")
    public void receiveUserCreated(AccountCreated accountCreated) {
        String email = accountCreated.getEmail();
        String username = accountCreated.getUsername();
        mailInformationRepository.save(
                mailInformationRepository.findById(username)
                        .orElseGet(() -> new MailInformation(username, email))
        );
        logger.info("Stored email address {} for user {}", email, username);
    }

    @JmsListener(destination = "Consumer.CommunicationMail.VirtualTopic.User:AccountDeleted",
            containerFactory = "queueListenerFactory")
    public void receiveUserDeleted(AccountDeleted accountDeleted) {
        try {
            mailInformationRepository.deleteById(accountDeleted.getUsername());
            logger.info("Deleted stored email address of user {}", accountDeleted.getUsername());
        } catch (EmptyResultDataAccessException e) {
            logger.info("No email address was stored for user {}", accountDeleted.getUsername());
        }

    }

}