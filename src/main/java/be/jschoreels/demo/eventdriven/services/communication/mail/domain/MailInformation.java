package be.jschoreels.demo.eventdriven.services.communication.mail.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("MailInformation")
public class MailInformation {

    @Id
    private String name;

    private String mail;

    public MailInformation(String name, String mail) {
        this.name = name;
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }
}
